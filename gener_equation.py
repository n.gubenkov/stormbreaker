# Additional code, used to generate theoretical data for some models, where possible

from const import *
from math import *

temp_range = range(2, 301)

def calc(temp):
    l = -80   # lambda
    y = l/(k*temp)
    return 0.1251*(8+(3*y-8)*exp(-1.5*y))/(y*(2+exp(-1.5*y)))


xt = ''
for i in temp_range:
    xt += str(i)+'\t'+str(calc(i))+'\n'
print(xt)
