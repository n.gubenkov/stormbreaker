from const import *
from math import exp
from diff_const import *

def chi_axis(temps,eigenvalues,H,ddH):
#    print eigenvalues
    mult = eigenvalues[0]
    axis_arr = eigenvalues[1]
    axis_arrd = eigenvalues[2]
    axis_arrf = []
    axis_arrfd = []
# To be rewritten
    for i in range(len(axis_arr)):
        axis_arrf.append(float(axis_arr[i]))
        axis_arrfd.append(float(axis_arrd[i]))
    minarrf = min(axis_arrf)
    for i in range(len(axis_arr)):
        axis_arrf[i] = axis_arrf[i]-minarrf
        axis_arrfd[i] = axis_arrfd[i]-minarrf
#    print zarrf
#    print zarrfd
    
    axis_chi = []
    for i in range(len(temps)):
        hsum = 0
        lsum = 0
        for j in range(len(axis_arr)):
            axis_cur = axis_arrf[j]
    #	    print zcur,
            axis_curd = axis_arrfd[j]
    #	    print zcur,zcurd,
            dE = (axis_cur-axis_curd)/(ddH*H) # -dEn/dH
    #	    print dE,
            hsum += dE*exp(-axis_cur/(k*temps[i]))
            lsum += exp(-axis_cur/(k*temps[i]))
    #	    print 'Failed on: zcur=',zcur,' temp=',temps[i]
        axis_chi.append(mult*(chi_const/H)*(hsum/lsum))
#    print mult
    
##    print z_chi	
##    if sym=='axial' or sym =='rhombic':
#	
    return axis_chi

def array_add(arr1,arr2,mult=1):
    res = []
    for i in range(len(arr1)):
        res.append(0)
    if len(arr1)!=len(arr2):
        raise Exception("array_add: Trying to add non-similar arrays")
    
    for i in range(len(arr1)):
        res[i] = arr1[i]+arr2[i]*mult

    return res

def chi_eq(temps, fragment, H, ddH,e_par):
    res = []
    ex_file = open("equation"+fragment+".py").read()
    exec(e_par,globals(),globals())
    exec(ex_file,globals(),globals())
    res = suspect(temps,H)
    return res

def chi(temps, eigenvalues, H, ddH, e_par, fragments):
    eig = eigenvalues[0:]

    chi_res = []
    for i in range(len(temps)):
        chi_res.append(0)

    for i in fragments:
        fin_line = i.strip()
        if fin_line!="" and fin_line[0:1] != "#":
            fin_recs = fin_line.split("\t")
    #	    print fin_line
            cur_fragment, cur_weight, cur_exec, cur_comment = fin_recs[0],fin_recs[1],fin_recs[2],fin_recs[3]
            if cur_exec.strip() == "*":
                chi_res = array_add(chi_res, chi_eq(temps,cur_fragment.strip(),H,ddH,e_par), mult = float(cur_weight))

    for i in eig:
        chi_res = array_add(chi_res, chi_axis(temps,i,H,ddH))

#    print chi_res
    return chi_res
# Rewrite
#    z_chi = chi_axis(temps, eigenvalues[:2],H,ddH)
#    if sym == 'isotropic':
#	return z_chi
#    else:
#	x_chi = chi_axis(temps, eigenvalues[2:4],H,ddH)
#	if sym == 'axial':
#	    ax_chi = []
#	    for i in range(len(z_chi)):
#		ax_chi.append((z_chi[i]+2.0*x_chi[i])/3.0)
#	    return ax_chi
#	else:
#	    y_chi = chi_axis(temps, eigenvalues[4:6],H,ddH)
#	    rh_chi = []
#	    for i in range(len(z_chi)):
#		rh_chi.append((z_chi[i]+x_chi[i]+y_chi[i])/3.0)
#	    return rh_chi
    pass
def chi_t(temps, chi_s, g=2, zJ = 0,pm_imp_g=2.0, pm_imp_s = 0.5, pm_imp_proc = 0, tip = 0 ):
    #     temps, chi_s, zJ_g, zJ,   par_g,        par_s,          par_p,           tip
    res = []
    for i in range(len(temps)):
        x_mf = temps[i]*chi_s[i]/(1-zJ*chi_s[i]*zJ_const/(g*g))
        xT = (1-pm_imp_proc)*(x_mf) + Nbk*pm_imp_g*pm_imp_g*pm_imp_proc*pm_imp_s*(pm_imp_s+1) + tip*temps[i]
        res.append(xT)
    return res

