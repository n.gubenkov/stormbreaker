# This is the module to define some constants

mu = 0.467 # Bohr magneton, cm-1/T
#Na = 6.022E+23 # Avogadro constant
k = 0.695039 # Bolzman constant, cm-1/K
chi_const = 1.195 # In calculation of chi from numeric derivatives of level energies
Nbk = 0.1251 # (N*mu^2)/(3k). Constant to calculate spin-only value: chi = 2*Nbk*S(S+1). 
# Or in some analytical expressions

zJ_const = 3.837 # Used in equation to calculate zJ correction