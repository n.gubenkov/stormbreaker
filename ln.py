# This is a module to generate matrices for single lanthanide ions.

import os
import sys

from launch import launch_parse


def gen_model(J):
    model_template = '''<?xml version="1.0" encoding="utf-8"?>
<model>
    <atoms>
        <atom number="1">
            <element>Ln</element>
            <spin_orbital>LS</spin_orbital>
            <s>%J%</s>
            <l>0</l>
            <local_model>axial</local_model>
        </atom>
    </atoms>
    <exchanges />
    <simplifications>
	<apply>gx1=gz1=gJ</apply>
    </simplifications>
</model>
'''
    model = model_template.replace("%J%", J)
    return model


def drop_model(model, fragment_num):
    fout = open("model"+fragment_num+".xml", "wb")
    fout.write(model)
    fout.close()


def J_from_Ln_name(ln_name):
    Ln_arr = {}
#   La is diamagnetic
    # Values are taken from Lueken-Kurs-SPP, charges are 3+ by default
    Ln_arr["Ce"] = "2.5"
    Ln_arr["Pr"] = "4"
    Ln_arr["Nd"] = "4.5"
    Ln_arr["Pm"] = "4"
#   Ln_arr["Sm"] = "2.5" --- Erased. More complicated model is needed for Sm
#   Ln_arr["Eu"] = "0" --- ZFS model is inappropriate at all.
#   Gd (L=0) is to be treated as "regular" ion (with LS scheme and S=3.5)
    Ln_arr["Tb"] = "6"
    Ln_arr["Dy"] = "7.5"
    Ln_arr["Ho"] = "8"
    Ln_arr["Er"] = "7.5"
    Ln_arr["Tm"] = "6"
    Ln_arr["Yb"] = "3.5"
#   Lu is diamagnetic
    try:
        return Ln_arr[ln_name]
    except:
        raise Exception("ln.py: J_from_Ln_name: "+ln_name +
                        " --- unrecognized or unsupported lanthanide.")


try:
    if len(sys.argv) < 4:
        raise Exception("ln.py: usage: " +
                        sys.argv[0]+" <ion> <model> <fragment number>")

    ln_name = sys.argv[1]
    ln_model = sys.argv[2]
    fragment_num = sys.argv[3]
    try:
        fn = int(fragment_num)
    except:
        raise Exception("ln.py: fragment number is not integer.")
    if fn < 0:
        raise Exception("ln.py: fragment number is not positive integer.")

#    print ln_name, ln_model, fragment_num
    if ln_name == "" or fragment_num == "":
        raise Exception(
            "ln.py: lanthanide ion or number of the fragment are not filled.")

    elif ln_model != 'ZFS':
        raise Exception(
            "ln.py: unsupported model, only ZFS one is implemented.")
    ln_J = J_from_Ln_name(ln_name)
    mod = gen_model(ln_J)
    drop_model(mod, fragment_num)
    launch_parse(fragment_num)
    os.unlink("model"+fragment_num+".xml")
except:
    print("Failed: ", sys.exc_info()[1])
    sys.exit(1)
