# Low-level matrix operations

import re


class ham_matrix:
    '''Describes a matrix pattern of the system\'s Hamiltonian'''
    LSJ_list = []  # List of spines, orbital or whole moment's, which determine level's energy: like: '1s,1l,2s'
    LSJ_values = []  # List of values from LSJ_list
    psi_func = []   # List of bra-cket element's like |1,1,3/2,1/2>
    length = 0      # Length of psi_func

    def __init__(self, LSJ_list):
        # I add 1 empty element to initial value to help cycle work correctly
        self.psi_func.append([])
        self.LSJ_list = LSJ_list.keys()
        self.LSJ_values = LSJ_list.values()

# self.LSJ_list.sort();
# We MUST NOT do this! Because we order keys, but don't order values => bugs!

        for key, value in LSJ_list.items():
            new_psi_func = []
            el_number = key[:-1]  # '1s' => key[:-1] == '1'
            el_type = key[-1:]  # '1s' => key[-1:] == 's'
            for i in self.psi_func:
                # if el_type=='l':
                # # This program doesn't allow calculate models with a few M_L values
                # # So we simply add element, for example [|1/2>,|-1/2>] + L=1/2 => [|1/2;1/2>,|-1/2;1/2>]
                # ------------ NO, this is not true, and is modified in 0.2 in order to allow direct LS model with L!=0 --------------
                # k = i[0:]
                # k.append(float(value))
                # new_psi_func.append(k)
                # else: # S or J => Multiplying current arrays to add S,S-1,...-S M_S/M_J values to each
                # So: [|1/2>,|-1/2>] + S=1/2 => [|1/2;1/2>,|1/2;-1/2>,|-1/2;1/2>,|-1/2;-1/2>].
                j = float(value)
                while j >= -float(value):
                    k = i[0:]  # Because "=" for lists means copying the reference
                    k.append(j)
                    new_psi_func.append(k)
                    j = j-1
            self.psi_func = new_psi_func
        self.length = len(self.psi_func)


def init_matrix(matrix_pattern):
    '''Creates matrix on the base of ham_matrix object'''
    l = matrix_pattern.length
    m = [['']*l for i in range(l)]
    return m


def pretty_element(elem):
    '''Simplifying the elements. [(-0.5)*mu*g*H+(1.5)*mu*g*H] =>[(1.0)*mu*g*H]'''
    elem_items = elem.split('+')
    pretty = []
    if len(elem_items) == 1:
        return elem
    else:
        items = {}
    # items_exprs = []
        for i in elem_items:
            res = re.search('\(([-]?[\d]+\.[\d]+)\)(\*[\*\w]+)', i)
            expr = res.group(2)
            num = res.group(1)

            if expr in items:
                items[expr] += float(num)
            else:
                items[expr] = float(num)
        for i in items.keys():
            if items[i] != 0:
                pretty.append('('+str(items[i])+')'+i)
        if len(pretty) == 0:
            return '0'
    return '+'.join(pretty)

#    return elem


def matrix_plus_matrix(m1, m2):
    '''Calculates the sum of given matrixes. If elements are numbers, makes numeric adding ("1"+"1" => "2").
    Strings will be concatenated: "(-0.5)*mu*g*H" + "(1.5)*mu*g*H" => "(-0.5)*mu*g*H+(1.5)*mu*g*H"'''
    l = len(m1)
    m = [['']*l for i in range(l)]
    for i in range(l):
        for j in range(l):
            if m1[i][j] == '' and m2[i][j] == '':
                m[i][j] = ''
            elif m1[i][j] == '':
                m[i][j] = m2[i][j]
            elif m2[i][j] == '':
                m[i][j] = m1[i][j]
            else:
                try:
                    f = float(m1[i][j])+float(m2[i][j])
                    if f == 0:
                        m[i][j] = ''
                    else:
                        m[i][j] = str(f)
                except Exception:
                    m[i][j] = m1[i][j]+'+'+m2[i][j]
    return m

# def matrix_plus_matrix_str(m1,m2):
#    l = len(m1)
#    m = [['']*l for i in range(l)]
#    for i in range(l):
# for j in range(l):
#            m[i][j] = m1[i][j]+'+'+m2[i][j]
#            print i,j
#    return m


def const_by_matrix(c, matrix):
    ''' Multiplies each element of the matrix by constant. If possible -- numerically, if not --
            "(elem)*const"'''
    l = len(matrix)
    t = False
    try:
        s = float(c)
        t = True
    except Exception:
        pass
    m = [['']*l for i in range(l)]
    for i in range(l):
        for j in range(l):
            if matrix[i][j] != '':
                if t:
                    try:
                        p = float(matrix[i][j])*s
            # if p == 0.0:
            # m[i][j] = ''
            # else:
                        m[i][j] = str(p)
                    except Exception:
                        m[i][j] = '('+matrix[i][j]+')*'+c
                else:
                    m[i][j] = '('+matrix[i][j]+')*'+c
    return m


def matrix_by_matrix(m1, m2):
    '''Matrixes multiplying. matrix_by_matrix calls must be replaced by direct calculation of each element 
    to optimize the calculation'''
    l = len(m1)
    mm = [[0]*l for i in range(l)]
    m = [['']*l for i in range(l)]
    for i in range(l):
        for j in range(l):
            for k in range(l):
                if m1[i][k] == '' or m2[k][j] == '':
                    mm[i][j] += 0
                else:
                    mm[i][j] += float(m1[i][k])*float(m2[k][j])
        # print i,j,k, mm
    for i in range(l):
        for j in range(l):
            if mm[i][j] == 0:
                m[i][j] = ''
            else:
                m[i][j] = str(mm[i][j])
    return m


def matrix_print(m, fn=''):
    '''Writing of the given matrix. By default -- to output, if fn specified -- to the file fn'''
    l = len(m)
    s = ''
    for i in range(l):
        s1 = ''  # Important! 's += elem' instruction, called often with large s, makes the code veeery slow
        for j in range(l):
            if m[i][j] == '':
                s1 += '0\t'
            else:
                s1 += pretty_element(m[i][j])+'\t'
        # s1+=m[i][j]+'\t'
        s += s1+'\n'
    # print i,j
    #!    s = "\n".join(map(lambda x:"\t".join(map(lambda y: pretty_element(y),m))))
    if fn == '':
        print(s)
    else:
        f = open(fn, 'w+')
        f.write(s)
        f.close()


def matrix_print_instructions(mname, m, fn=''):
    '''Writing instructions to build the given matrix. By default -- to output, if fn specified -- to the file fn'''
    l = len(m)
    s_arr = []
    s = mname+' = [[0]*'+str(l)+' for i in range('+str(l)+')]\n'
    for i in range(l):
        for j in range(l):
            if m[i][j] != '':
                s_arr.append(mname+'['+str(i)+']['+str(j) +
                             '] = '+pretty_element(m[i][j]))
    # s+=mname+'['+str(i)+']['+str(j)+'] = '+m[i][j]+'\n'
    s += "\n".join(s_arr)
    if fn == '':
        print(s)
    else:
        f = open(fn, 'w+')
        f.write(s)
        f.close()
