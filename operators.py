from matrix import *
from math import *


# Elementary operators
# t = s, l or j (type of quantum number), num -- number, matrix_pattern -- ham_matrix object for this system
def z2(t, num, matrix_pattern):
    m = init_matrix(matrix_pattern)
    lsj = matrix_pattern.LSJ_list
    ps = matrix_pattern.psi_func
    l = matrix_pattern.length
    el = str(num)+t
    affected = list(lsj).index(el)
    for i in range(l):
        print(1)
        m[i][i] = str(float(ps[i][affected])**2)
    return m


def z(t, num, matrix_pattern):
    m = init_matrix(matrix_pattern)
    lsj = matrix_pattern.LSJ_list
    ps = matrix_pattern.psi_func
    l = matrix_pattern.length
    el = str(num)+t
    affected = list(lsj).index(el)

    for i in range(l):
        m[i][i] = str(ps[i][affected])
    return m


def find_ps_differ(psi, psj, num, sign):
    res = True
    for i in range(len(psi)):
        if i != num and psi[i] != psj[i]:
            res = False
            break
        elif i == num and float(psi[i])-float(psj[i]) != sign:
            res = False
            break
    return res


def find_ps_2_differs(psi, psj, num1, num2, sign1, sign2):
    res = True
    for i in range(len(psi)):
        if i != num1 and i != num2 and psi[i] != psj[i]:
            res = False
            break
        elif i == num1 and float(psi[i])-float(psj[i]) != sign1:
            res = False
            break
        elif i == num2 and float(psi[i])-float(psj[i]) != sign2:
            res = False
            break
    return res


def plusminus(t1, num1, t2, num2, matrix_pattern):
    m = init_matrix(matrix_pattern)
    lsj = matrix_pattern.LSJ_list
    lsjv = matrix_pattern.LSJ_values
    ps = matrix_pattern.psi_func
    l = matrix_pattern.length
    el1 = str(num1)+t1
    affected1 = list(lsj).index(el1)
    el2 = str(num2)+t2
    affected2 = list(lsj).index(el2)
    
    LSJ1 = float(list(lsjv)[affected1])
    LSJ2 = float(list(lsjv)[affected2])
    for i in range(l):
        for j in range(l):

            if find_ps_2_differs(ps[i], ps[j], affected1, affected2, 1, -1):
                mlsj1 = ps[j][affected1]
                mlsj2 = ps[j][affected2]
        # print 'mls', mls
        # print sqrt(LS*(LS+1) - mls*(mls+1))
                m[i][j] = str(sqrt(LSJ1*(LSJ1+1) - mlsj1*(mlsj1+1))
                              * sqrt(LSJ2*(LSJ2+1) - mlsj2*(mlsj2-1)))
    return m


def minusplus(t1, num1, t2, num2, matrix_pattern):
    m = init_matrix(matrix_pattern)
    lsj = matrix_pattern.LSJ_list
    lsjv = matrix_pattern.LSJ_values
    ps = matrix_pattern.psi_func
    l = matrix_pattern.length
    el1 = str(num1)+t1
    affected1 = list(lsj).index(el1)
    el2 = str(num2)+t2
    affected2 = list(lsj).index(el2)
    LSJ1 = float(list(lsjv)[affected1])
    LSJ2 = float(list(lsjv)[affected2])
    for i in range(l):
        for j in range(l):
            if find_ps_2_differs(ps[i], ps[j], affected1, affected2, -1, 1):
                mlsj1 = ps[j][affected1]
                mlsj2 = ps[j][affected2]
        # print 'mls', mls
        # print sqrt(LS*(LS+1) - mls*(mls+1))
                m[i][j] = str(sqrt(LSJ1*(LSJ1+1) - mlsj1*(mlsj1-1))
                              * sqrt(LSJ2*(LSJ2+1) - mlsj2*(mlsj2+1)))
    return m


def plus(t, num, matrix_pattern):
    m = init_matrix(matrix_pattern)
    lsj = matrix_pattern.LSJ_list
    lsjv = matrix_pattern.LSJ_values
    ps = matrix_pattern.psi_func
    l = matrix_pattern.length
    el = str(num)+t
    affected = list(lsj).index(el)
    LSJ = float(list(lsjv)[affected])
    for i in range(l):
        for j in range(l):
            if find_ps_differ(ps[i], ps[j], affected, 1):
                print(5)
                mlsj = ps[j][affected]
    # print 'mls', mls
    # print sqrt(LS*(LS+1) - mls*(mls+1))
                m[i][j] = str(sqrt(LSJ*(LSJ+1) - mlsj*(mlsj+1)))
    return m


def minus(t, num, matrix_pattern):
    m = init_matrix(matrix_pattern)
    lsj = matrix_pattern.LSJ_list
    lsjv = matrix_pattern.LSJ_values
    ps = matrix_pattern.psi_func
    l = matrix_pattern.length
    el = str(num)+t
    affected = list(lsj).index(el)
    LSJ = float(list(lsjv)[affected])
    for i in range(l):
        for j in range(l):
            print(6)
            if find_ps_differ(ps[i], ps[j], affected, -1):
                mlsj = ps[j][affected]
            m[i][j] = str(sqrt(LSJ*(LSJ+1) - mlsj*(mlsj-1)))
    return m


def z1z2(t1, num1, t2, num2, matrix_pattern):
    m = init_matrix(matrix_pattern)
    lsj = matrix_pattern.LSJ_list
    ps = matrix_pattern.psi_func
    l = matrix_pattern.length
    el1 = str(num1)+t1
    el2 = str(num2)+t2
    affected1 = list(lsj).index(el1)
    affected2 = list(lsj).index(el2)
#    print lsj, el
    for i in range(l):
        m[i][i] = str(ps[i][affected1]*ps[i][affected2])

    return m
