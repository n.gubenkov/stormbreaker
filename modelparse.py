from config import dump_full_matrices
from os import umask, chmod, access, unlink, F_OK, R_OK
from xml.dom.minidom import *
from operators import *
from time import *
import sys
# from matrix import *
import gettext
gettext.textdomain('Stormbreaker')
_ = gettext.gettext

# --------------------------------------------------------------
# Defining global variables
# --------------------------------------------------------------

LSJ_list = {}
psi_func = {}
subst_list = {}
zeeman_symmetry = 'isotropic'
proc_atoms = []
proc_exs = []
param_list = []
total_l = 0
file_suffix = ""
param_prefix = ""

# --------------------------------------------------------------
# Defining some functions
# --------------------------------------------------------------


def pl_add(el):
    if not el in param_list:
        param_list.append(el)


def check_pos_one_half(s):
    ''' Values of quantum numbers S, L or J have to be integer or half-integer positive numbers.
    This function checks, if they are.'''
    a = float(s)
    if a < 0:
        raise Exception(_('Negative data'))
    if 2*a - round(2*a) != 0:
        raise Exception(_('Non one-half data'))
    return s


def preparse_atoms_list(atoms_list):
    global zeeman_symmetry, total_l
    j = 0
    for i in atoms_list:
        j += 1
        atom = {}
        atom['element'] = i.getElementsByTagName(
            'element')[0].childNodes[0].data
        number = i.getAttribute('number')
        if int(number) != j:
            raise Exception(
                _('Atoms have to be numbered in order of description'))
        atom['local_model'] = i.getElementsByTagName(
            'local_model')[0].childNodes[0].data
        if atom['local_model'] == 'isotropic':
            pass
        elif atom['local_model'] == 'axial':
            if zeeman_symmetry == 'isotropic':
                zeeman_symmetry = 'axial'
        elif atom['local_model'] == 'rhombic':
            #    	    zeeman_symmetry = 'rhombic'
            raise Exception(
                _('Atom ')+str(j)+_(': sorry, rhombic symmetry is not yet implemented'))
        else:
            raise Exception(_('Atom ')+str(j) +
                            _(': wrong local model requested'))
        atom['spin_orbital'] = i.getElementsByTagName(
            'spin_orbital')[0].childNodes[0].data
        if (atom['spin_orbital'] == 'LS'):
            atom['s'] = check_pos_one_half(
                i.getElementsByTagName('s')[0].childNodes[0].data)
            atom['l'] = check_pos_one_half(
                i.getElementsByTagName('l')[0].childNodes[0].data)
            LSJ_list[number+'s'] = atom['s']
            if atom['l'] != '0':
                LSJ_list[number+'l'] = atom['l']
                total_l += int(atom['l'])

        elif (atom['spin_orbital'] == 'JJ'):
            raise Exception(_('JJ model is not yet implemented, sorry'))
        else:
            raise Exception(_('Atom ')+str(j) +
                            _(': wrong spin-orbital model requested'))
        proc_atoms.append(atom)


def z_zeeman():
    l = len(proc_atoms)
    m = psi_func.length
    z_zee = [['']*m for i in range(m)]
    for i in range(1, l+1):
        g_factor = param_prefix+'gz'+str(i)
        if g_factor in subst_list:
            g_factor = subst_list[g_factor]
        pl_add(g_factor)
        z_zee = matrix_plus_matrix(z_zee, const_by_matrix(
            'mu*Hz*'+g_factor, z('s', i, psi_func)))
        L = int(proc_atoms[i-1]['l'])
        if L != 0:
            eta = param_prefix+'eta'+str(i)
            if eta in subst_list:
                eta = subst_list[eta]
            pl_add(eta)
            z_zee_l = const_by_matrix('mu*Hz*'+eta, z('l', i, psi_func))
            pass
            z_zee = matrix_plus_matrix(z_zee, z_zee_l)
        print(_('Calculated z Zeeman matrix number ')+str(i) +
              _(' in ')+str(time()-run_start)+_(' seconds'))
    return z_zee


def x_zeeman():
    l = len(proc_atoms)
    m = psi_func.length
    x_zee = [['']*m for i in range(m)]
    for i in range(1, l+1):
        g_factor = param_prefix+'gx'+str(i)
        if g_factor in subst_list:
            g_factor = subst_list[g_factor]
            pl_add(g_factor)
        x_el = matrix_plus_matrix(
            plus('s', i, psi_func), minus('s', i, psi_func))
        x_el = const_by_matrix('0.5', x_el)
        x_zee = matrix_plus_matrix(
            x_zee, const_by_matrix('mu*Hx*'+g_factor, x_el))
        L = int(proc_atoms[i-1]['l'])
        if L != 0:
            eta = param_prefix+'eta'+str(i)
            if eta in subst_list:
                eta = subst_list[eta]

            x_el_l = matrix_plus_matrix(
                plus('l', i, psi_func), minus('l', i, psi_func))
            x_el_l = const_by_matrix('0.5', x_el_l)
            x_zee_l = const_by_matrix('mu*Hz*'+eta, x_el_l)
            pass
            x_zee = matrix_plus_matrix(x_zee, x_zee_l)
        print(_('Calculated x Zeeman matrix number ')+str(i) +
              _(' in ')+str(time()-run_start)+_(' seconds'))
        return x_zee


def LS():
    l = len(proc_atoms)
    m = psi_func.length
    LS_mat = [['']*m for i in range(m)]
    for i in range(1, l+1):
        if proc_atoms[i-1]['l'] != '0':
            _lambda = param_prefix+'lambda'+str(i)
            if _lambda in subst_list:
                _lambda = subst_list[_lambda]
            pl_add(_lambda)
            eta = param_prefix+'eta'+str(i)
            if eta in subst_list:
                eta = subst_list[eta]

            mul_z = z1z2('l', i, 's', i, psi_func)

            mul_xy = const_by_matrix("0.5", matrix_plus_matrix(
                plusminus('l', i, 's', i, psi_func), minusplus('l', i, 's', i, psi_func)))

            mul = matrix_plus_matrix(mul_z, mul_xy)
            LS_mat = matrix_plus_matrix(
                LS_mat, const_by_matrix(_lambda+'*'+eta, mul))
            print(_('LS matrix for atom ')+str(i)+_(' calculated in ') +
                  str(time()-run_start)+_(' seconds'))
    return LS_mat


def ZFS():
    l = len(proc_atoms)
    m = psi_func.length
    LS_m = [['']*m for i in range(m)]
    for i in range(1, l+1):
        if proc_atoms[i-1]['l'] != '0':
            # Operator delta_Lz^2, part of LS operator
            delta = param_prefix+'delta'+str(i)
            if delta in subst_list:
                delta = subst_list[delta]
            pl_add(delta)
            LS_m = matrix_plus_matrix(
                LS_m, const_by_matrix(delta, z2('l', i, psi_func)))
        elif proc_atoms[i-1]['local_model'] == 'axial' or proc_atoms[i-1]['local_model'] == 'rhombic':
            D = param_prefix+'D'+str(i)
            if D in subst_list:
                D = subst_list[D]
                pl_add(D)
                LS_m = matrix_plus_matrix(
                    LS_m, const_by_matrix(D, z2('s', i, psi_func)))
    return LS_m


def preparse_exchanges_list(ex_list):
    j = 0
    for i in ex_list:
        j += 1
        ex = {}
        number = i.getAttribute('number')
        if int(number) != j:
            raise Exception(
                _('Exchanges have to be numbered in order of description'))
        ex['atom1'] = i.getElementsByTagName('atom1')[0].childNodes[0].data
        ex['atom2'] = i.getElementsByTagName('atom2')[0].childNodes[0].data
    # Add checking if such atoms exist
        ex['ex_model'] = i.getElementsByTagName(
            'ex_model')[0].childNodes[0].data
        proc_exs.append(ex)
    pass


def exchange():
    l = len(proc_exs)
    m = psi_func.length
    exs = [['']*m for i in range(m)]
    for i in range(l):
        a1 = proc_exs[i]['atom1']
        a2 = proc_exs[i]['atom2']
        if proc_exs[i]['ex_model'] == 'Ising':
            J = param_prefix+'Jz'+str(i+1)
            if J in subst_list:
                J = subst_list[J]
            pl_add(J)
            mul = matrix_by_matrix(z('s', a1, psi_func), z('s', a2, psi_func))
            mul = const_by_matrix('-2', mul)
            exs = matrix_plus_matrix(exs, const_by_matrix(J, mul))
        elif proc_exs[i]['ex_model'] == 'Heisenberg':
            J = param_prefix+'Jz'+str(i+1)
            if J in subst_list:
                J = subst_list[J]
            pl_add(J)
            mul_z = z1z2('s', a1, 's', a2, psi_func)
            mul_z = const_by_matrix('-2', mul_z)
            mul_xy = matrix_plus_matrix(
                plusminus('s', a1, 's', a2, psi_func), minusplus('s', a1, 's', a2, psi_func))
            mul_xy = const_by_matrix('-1', mul_xy)
            mul = matrix_plus_matrix(mul_z, mul_xy)
            exs = matrix_plus_matrix(exs, const_by_matrix(J, mul))
        elif proc_exs[i]['ex_model'] == 'General':
            raise Exception(
                _('General exchange model is not yet implemented, sorry'))
        else:
            raise Exception(_('Exchange')+str(i) +
                            _(': wrong exchange model requested'))
        print(_('Exchange matrix for exchange ')+str(i) +
              _(' calculated in ')+str(time()-run_start)+_(' seconds'))

    return exs


def ham_print(psi_func, f=''):
    s = '|'
    for i in psi_func.LSJ_list:
        s += 'M'+i[-1:]+i[:-1]+';'
    s += '> \n'
    for i in psi_func.psi_func:
        s += '|'
        for j in i:
            s += str(j)+';'
        s += '> '

    if f == '':
        print(s)
    else:
        fp = open(f, 'w+')
        fp.write(s)
        fp.close


def parse_matrix(name):
    f = open(name, 'rb')
    out = ""
    i = 0
    j = 0
    while 1:
        fl = f.readline().strip()
        if not fl:
            break
        fla = fl.split('\t')
    # print fla
        for x in fla:
            if x != '0':
                out += 'm'+name+'['+str(i)+']['+str(j)+'] = '+x+'\n'
            j += 1
        i += 1
        j = 0
    # Command to create empty matrix
    out = 'm'+name+' = [[0]*'+str(i)+' for i in range('+str(i)+')]\n'+out
    fw = open(name+'.inc', 'wb')
    fw.write(out)
    fw.close()
    f.close()

# --------------------------------------------------------------
# Main part of program
# --------------------------------------------------------------


try:
    # Including psyco module to accelerate the program

    run_start = time()
    print(_('Started model parsing at ')+asctime(localtime()))

    # TODO: dp
    if len(sys.argv) == 1:
        file_suffix = '0'
        param_prefix = "m0_"

    elif len(sys.argv) > 1:
        if int(sys.argv[1]) < 0:
            raise Exception("Parameter 1 must be integer > 0")
        file_suffix = sys.argv[1].strip()
        param_prefix = "m"+sys.argv[1].strip()+"_"

    config = parse('model'+file_suffix+'.xml')
    atoms = config.getElementsByTagName('atoms')[0]
    atoms_list = atoms.getElementsByTagName('atom')
    exchanges = config.getElementsByTagName('exchanges')[0]
    exchanges_list = exchanges.getElementsByTagName('exchange')
    simplifications = config.getElementsByTagName('simplifications')[0]
    simplifications_list = simplifications.getElementsByTagName('apply')

# I'll prepare simplification list
    for i in simplifications_list:
        s = i.childNodes[0].data.split('=')
        sub_arr = s[:-1]
        sub = s[-1:]
        for i in sub_arr:
            subst_list[param_prefix+i] = param_prefix+sub[0]

    preparse_atoms_list(atoms_list)
    preparse_exchanges_list(exchanges_list)

    psi_func = ham_matrix(LSJ_list)

    general_matrix = exchange()
    if total_l != 0:
        general_matrix = matrix_plus_matrix(general_matrix, LS())

    if zeeman_symmetry != 'isotropic':
        general_matrix = matrix_plus_matrix(general_matrix, ZFS())
        x_matrix = matrix_plus_matrix(general_matrix, x_zeeman())
        if dump_full_matrices == 1:
            matrix_print(x_matrix, 'x'+file_suffix)
        matrix_print_instructions(
            "mx"+file_suffix, x_matrix, 'x'+file_suffix+'.inc')
        print(_('Printed x-matrix in ')+str(time()-run_start)+_(' seconds'))
#        if zeeman_symmetry == 'rhombic':
# y_matrix = matrix_plus_matrix(general_matrix,y_zeeman())
# matrix_print(y_matrix,'y')
# print _('Printed y-matrix in ')+str(time()-run_start)+_(' seconds')
    z_matrix = matrix_plus_matrix(general_matrix, z_zeeman())
    if dump_full_matrices == 1:
        matrix_print(z_matrix, 'z'+file_suffix)
    matrix_print_instructions(
        "mz"+file_suffix, z_matrix, 'z'+file_suffix+'.inc')

    print(_('Printed z-matrix in ')+str(time()-run_start)+_(' seconds'))

    if dump_full_matrices == 1:
        ham_print(psi_func, 'eigenfunctions'+file_suffix)

# Preparing parameters list
    p_l = open('param_list'+file_suffix, 'w+')
    p_l_s = ','.join(param_list)
    p_l.write(p_l_s)
    p_l.close()
    sym = open('symmetry'+file_suffix, 'w+')
    sym.write(zeeman_symmetry)
    sym.close()

    print(_('Printed symmetry and eigenfuctions in ') +
          str(time()-run_start)+_(' seconds'))

    print(_('Finished parsing the model in ') +
          str(time()-run_start)+_(' seconds'))
except:
    print(_('Failed: '), sys.exc_info()[1])
