# This module converts matrix, stored in file, into a set of python instructions to fill it. Zeroes are ommited:
# (1  0)    m[0][0] = 1
# (0 -1) => m[1][1] = -1
#

from os import access, R_OK, umask, chmod

umask(0)


def parse_matrix(name):
    f = open(name, 'rb')
    out = ""
    i = 0
    j = 0
    while 1:
        fl = f.readline().strip()
        if not fl:
            break
        fla = fl.split('\t')
    # print fla
        for x in fla:
            if x != '0':
                out += 'm'+name+'['+str(i)+']['+str(j)+'] = '+x+'\n'
            j += 1
        i += 1
        j = 0
        # Command to create empty matrix
        out = 'm'+name+' = [[0]*'+str(i)+' for i in range('+str(i)+')]\n'+out
        fw = open(name+'.inc', 'wb')
        fw.write(out)
        fw.close()
        f.close()


if access('z', R_OK):
    parse_matrix('z')
else:
    raise Exception(_('No input "z" matrix, stopping'))
if access('x', R_OK):
    parse_matrix('x')
if access('y', R_OK):
    parse_matrix('y')
