import prepare_plot from gmagn
import numpy as np

{
    'm0_JNiSQ' : np.arange(-100,100, 0.01),
    'm0_JSQSQ' : np.arange(-10,10, 0.01),
    'm0_gzNi'  : np.arange(2.0,2.5, 0.01),
    'm0_gSQ'   : np.arange(2.0,2.1, 0.01),
    'H'        : np.arange(0.5, 0.6, 0.01)
}

ddH = 0.0001
array_to_return = 1

prepare_plot()