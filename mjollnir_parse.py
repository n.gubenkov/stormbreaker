import sys
from os import access, R_OK
from launch import launch, launch_parse


def ext_parser(instruction, num):
    if instruction[:2] == 'Ln':
        instr_set = instruction[2:].split(" ")
        params = []
        for i in instr_set:
            s = i.strip()
            if s != "":
                params.append(s)
        if len(params) < 2:
            raise Exception("mjollnir_parse.py: ext_parser: fragment "+num +
                            ": the instruction misses lanthanide and/or model name.")

        ln_names = ["Ce", "Pr", "Nd", "Pm", "Tb", "Dy", "Ho", "Er", "Tm", "Yb"]
        if params[0] not in ln_names:
            raise Exception("mjollnir_parse.py: ext_parser: "+params[0] +
                            " --- unrecognized or unsupported lanthanide.")
        if params[1] != "ZFS":
            raise Exception(
                "mjollnir_parse.py: ext_parser: unsupported model, only ZFS one is implemented")
        launch(["ln.py", params[0], "ZFS", num])

    else:
        raise Exception("mjollnir_parse.py: ext_parser: unsupported parser")
    pass


try:
    frag_raw = open("fragments").readlines()
    fragments = []
    for i in frag_raw:
        ln = i.strip()
        if ln[0:1] == "#" or ln == "":
            pass
        else:
            fragments.append(i)
    c = 0
    for j in fragments:
        num, weight, flag, comment = j.strip().split("\t")
        num = num.strip()
        weight = weight.strip()
        flag = flag.strip()
        comment = comment.strip()
        fname = 'model'+num+".xml"

        if int(num) != c:
            print(c, num)
            raise Exception(
                "Submodels are supposed to be numbered sequentially from 0")
        if flag == "+":
            if comment[:1] == "!":
                # Comment is treated as instruction to external parser
                ext_parser(comment[1:].strip(), num)
            elif access(fname, R_OK):
                launch_parse(num)
            else:
                raise Exception("No model file "+fname)
        c += 1
except:
    print("Failed: ", sys.exc_info()[1])
