mz0 = [[0]*8 for i in range(8)]
mz0[0][0] = (-1.0)*m0_JCuSQ+(-0.5)*m0_JSQSQ+(0.5)*mu*Hz*m0_gzCu+(1.0)*mu*Hz*m0_gSQ
mz0[1][1] = (0.5)*m0_JSQSQ+(0.5)*mu*Hz*m0_gzCu
mz0[1][2] = (-1.0)*m0_JSQSQ
mz0[1][4] = (-1.0)*m0_JCuSQ
mz0[2][1] = (-1.0)*m0_JSQSQ
mz0[2][2] = (0.5)*m0_JSQSQ+(0.5)*mu*Hz*m0_gzCu
mz0[2][4] = (-1.0)*m0_JCuSQ
mz0[3][3] = (1.0)*m0_JCuSQ+(-0.5)*m0_JSQSQ+(0.5)*mu*Hz*m0_gzCu+(-1.0)*mu*Hz*m0_gSQ
mz0[3][5] = (-1.0)*m0_JCuSQ
mz0[3][6] = (-1.0)*m0_JCuSQ
mz0[4][1] = (-1.0)*m0_JCuSQ
mz0[4][2] = (-1.0)*m0_JCuSQ
mz0[4][4] = (1.0)*m0_JCuSQ+(-0.5)*m0_JSQSQ+(-0.5)*mu*Hz*m0_gzCu+(1.0)*mu*Hz*m0_gSQ
mz0[5][3] = (-1.0)*m0_JCuSQ
mz0[5][5] = (0.5)*m0_JSQSQ+(-0.5)*mu*Hz*m0_gzCu
mz0[5][6] = (-1.0)*m0_JSQSQ
mz0[6][3] = (-1.0)*m0_JCuSQ
mz0[6][5] = (-1.0)*m0_JSQSQ
mz0[6][6] = (0.5)*m0_JSQSQ+(-0.5)*mu*Hz*m0_gzCu
mz0[7][7] = (-1.0)*m0_JCuSQ+(-0.5)*m0_JSQSQ+(-0.5)*mu*Hz*m0_gzCu+(-1.0)*mu*Hz*m0_gSQ