import sys
import subprocess


def launch(command_line):
    if sys.platform == "win32":
        subprocess.call(command_line, shell=True)
    else:
        subprocess.call(command_line)


def launch_parse(num):
    print("================ Start parsing of model "+num+" ================")
    launch(["modelparse.py", num])
    print("================ End parsing of model "+num+"   ================")
