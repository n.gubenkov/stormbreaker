import csv
import glob
from tkinter import *
from tkinter import ttk
from tkinter.messagebox import showerror, showinfo, askyesno
import sys
import math
from threading import Thread
import json

from eigen_get import *
from suspect import *
from diff_const import *
import os
from shutil import rmtree, copy
import gettext

import numpy as np
gettext.textdomain('stormbreaker')
_ = gettext.gettext


pic_size = [500, 500]  # Canvas size: x,y
x_coords = [0, 300.0, 50.0]  # start, end, distance between labels
y_coords = [0, 30.0, 3.0]
# Initial values. Really maximum must be user selected

plot_padding = 100  # Padding around the real diagram
small_line = 5

# "diagram" objects array to be drawn. plot_arrays[0] contains experimental data,
plot_arrays = []
# other elements -- calculated ones

source_array = []

saved_fits_number = 0  # number of files, saved in `fits' directory

param_frames = {}  # Widgets with parameters


# Global paramters
fragments = None
temps = None
source = None
symmetry = {}
param_list = []
exec_list = []
exec_file_list = []
matrix_name_list = []
parameters = {}
number_of_threads = 2


def phys_x(logic_x):
    '''Converts logical coordinate x to th physical one for drawing on the canvas'''
    phys_range = float(pic_size[0]) - 2*plot_padding
    logic_range = float(x_coords[1]-x_coords[0])
    return plot_padding + float(logic_x - x_coords[0])*phys_range/logic_range


def phys_y(logic_y):
    '''Converts logical coordinate y to th physical one for drawing on the canvas'''
    phys_range = float(pic_size[1]) - 2*plot_padding
    logic_range = float(y_coords[1]-y_coords[0])
    return pic_size[1] - plot_padding - float(logic_y - y_coords[0])*phys_range/logic_range


class point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class diagram:
    '''Describes some set of points and diagram color'''

    def __init__(self, color='black'):
        self.points = []
        self.color = color

    def append(self, x, y):
        self.points.append(point(x, y))

    def save():
        pass


class magn_param:
    '''Create label and input in UI'''

    def __init__(self, top, name, is_range=False):
        if is_range:
            self.range_constructor(top, name)
        else:
            self.constructor(top, name)

    def range_constructor(self, top, name):
        self.frame = Frame(top)
        self.element_label = Label(self.frame, text=name, font=used_font)
        self.from_label = Label(self.frame, text='From', font=used_font)
        self.to_label = Label(self.frame, text='To', font=used_font)
        self.step_label = Label(self.frame, text='Step', font=used_font)

        self.from_entry = Entry(self.frame, font=used_font)
        self.to_entry = Entry(self.frame, font=used_font)
        self.step_entry = Entry(self.frame, font=used_font)
        self.frame.pack(side=TOP, expand=YES)
        self.element_label.pack(side=LEFT, expand=YES)
        self.from_label.pack(side=LEFT, expand=YES)
        self.from_entry.pack(side=LEFT, expand=YES)
        self.to_label.pack(side=LEFT, expand=YES)
        self.to_entry.pack(side=LEFT, expand=YES)
        self.step_label.pack(side=LEFT, expand=YES)
        self.step_entry.pack(side=LEFT, expand=YES)

    def constructor(self, top, name):
        self.frame = Frame(top)
        self.element_label = Label(self.frame, text=name, font=used_font)
        self.from_entry = Entry(self.frame, font=used_font)
        self.frame.pack(side=TOP, expand=YES)
        self.element_label.pack(side=LEFT, expand=YES)
        self.from_entry.pack(side=LEFT, expand=YES)


def plot_axes():
    ''' This function prints axes and labels on them'''
    pic.delete('axes')
    canvas_font = ('times', small_line, 'normal')
    # Ox
    startx = phys_x(x_coords[0])
    starty = phys_y(y_coords[0])
    endx = phys_x(x_coords[1])
    endy = phys_y(y_coords[0])
    pic.create_line(startx, starty, endx, endy, tag='axes')
    i = 0
    while x_coords[0]+i*x_coords[2] < x_coords[1]:
        endx = startx = phys_x(x_coords[0]+i*x_coords[2])
        starty = phys_y(y_coords[0])+small_line
        endy = phys_y(y_coords[0])-small_line
        pic.create_line(startx, starty, endx, endy, tag='axes')
        if i != 0:
            pic.create_text(startx, phys_y(y_coords[0])+2*small_line, text=str(
                x_coords[0]+i*x_coords[2]), font=canv_font, anchor=N, tag='axes')
        i += 1
    i = 0

    pic.create_text(phys_x(x_coords[1]), phys_y(
        y_coords[0])+2*small_line, text='T', font=canv_font, anchor=N, tag='axes')
    # Oy
    startx = phys_x(x_coords[0])
    starty = phys_y(y_coords[0])
    endx = phys_x(x_coords[0])
    endy = phys_y(y_coords[1])
    pic.create_line(startx, starty, endx, endy, tag='axes')
    i = 0
    while y_coords[0]+i*y_coords[2] < y_coords[1]:
        startx = phys_x(x_coords[0])-small_line
        endy = starty = phys_y(y_coords[0]+i*y_coords[2])
        endx = phys_x(x_coords[0])+small_line
        pic.create_line(startx, starty, endx, endy, tag='axes')
        if i != 0:
            pic.create_text(phys_x(x_coords[0])-2*small_line, starty,
                            text=f'{y_coords[0]+i*y_coords[2]:g}', font=canv_font, anchor=E, tag='axes')
        i += 1
    i = 0

    pic.create_text(phys_x(0)-2*small_line,
                    phys_y(y_coords[1]), text='χT', font=canv_font, anchor=E, tag='axes')
    pic.create_text(phys_x(0)-2*small_line, phys_y(0)+2*small_line,
                    text='0', font=canv_font, anchor=NE, tag='axes')


def prepare_plot(plot_arrays):
    '''Calculate data for given set of parameters and add it to \'plot_arrays\''''
    global number_of_threads
    global number_of_combinations
    global combintations_calculated
    global progress_bar

    combintations_calculated = 0

    try:
        ddH = math.pow(10, (-int(ex_param_frames['ddH'].get())))
    except:
        showerror(_('Error'), _('Inappropriate parameter -lg(ddH/H): ') +
                  ex_param_frames['ddH'].get())

    array_to_return = convert_into_2d_array_of_values(param_frames)
    elements = list(param_frames.keys())

    number_of_combinations = math.prod([len(x) for x in array_to_return])

    progress_bar['maximum'] = number_of_combinations

    if len(array_to_return[0]) < number_of_threads:
        # if number of elements less than number of threads set number of threads equal to number of elements
        number_of_threads = len(array_to_return[0])

    splits_of_first_array = np.array_split(
        array_to_return[0], number_of_threads)

    splits = []
    for splited in splits_of_first_array:
        splits.append([splited, *array_to_return[1:]])

    best_fit_coordinates = find_best_fit(
        number_of_threads, elements, plot_arrays, ddH, splits)
    return best_fit_coordinates


def find_best_fit(number_of_threads, elements, plot_arrays, ddH, splits, temp_best_fit=None, current_iterations=None):
    thread_return_value = [None]*number_of_threads
    global additional_parameters

    threads = [Thread(target=calculation, args=(array_to_run, elements, plot_arrays, ddH, additional_parameters, ind, thread_return_value, current_iterations[ind] if current_iterations else None))
               for ind, array_to_run in enumerate(splits)]

    for thread in threads:
        thread.start()

    # wait for the threads to complete
    [t.join() for t in threads]

    best_fit, best_fit_coordinates, best_fit_combination, mapped_combination, best_additional_parameters = min(
        thread_return_value, key=lambda x: x[0])  # find best fit in all threads results

    if temp_best_fit:
        best_fit = min(best_fit, temp_best_fit)

    l_r.config(text='R2 = '+str(best_fit))
    save_fit(best_fit_coordinates, mapped_combination,
             best_additional_parameters)
    return best_fit_coordinates


def generate_nested_for_loops(ind, array_to_return):
    if ind == len(array_to_return):
        return ""

    loop_code = f"for elementes_of_arr{ind} in array_to_return[{ind}]:\n"
    nested_code = generate_nested_for_loops(ind + 1, array_to_return)

    return loop_code + nested_code


def calculation(array_to_return, elements, plot_arrays, ddH, additional_parameters, ind=None, thread_return_value=None, search_value=None):
    global number_of_combinations
    global combintations_calculated
    global progress_bar
    global root
    global best_fit
    global best_fit_coordinates
    global best_fit_combination
    global best_additional_parameters
    global mapped_combination
    global combination_iterator
    global correlation
    temp_save_every = 10

    combination_iterator = 0
    best_fit = None
    best_fit_coordinates = None
    best_fit_combination = None
    mapped_combination = None
    best_additional_parameters = None
    correlation = 0
    ind += 1
    # test_nested = generate_nested_for_loops(0,array_to_return)
    print(f"Runnig thread {ind}")

    def nested_iteration(arr, depth, current_values, additional_parameters, search_value=None):
        global number_of_combinations
        global combintations_calculated
        global progress_bar
        global root
        global best_fit
        global best_fit_coordinates
        global best_fit_combination
        global best_additional_parameters
        global mapped_combination
        global combination_iterator
        global correlation

        if depth == len(array_to_return):
            # TODO: check if search_value >= current_values does proper check
            if not (search_value and search_value >= current_values):
                correlation, coordinates, temp_additional_parameters = calculate_combination(
                    current_values, elements, plot_arrays, additional_parameters, ddH, ind=ind)

                if not best_fit or correlation < best_fit:
                    best_fit = correlation
                    best_fit_coordinates = coordinates
                    best_fit_combination = current_values
                    best_additional_parameters = temp_additional_parameters
                    mapped_combination = dict(
                        zip(elements, current_values))
                combination_iterator += 1

                combintations_calculated += 1
                print(
                    f'Progress {combintations_calculated} / {number_of_combinations} \n')

                if combination_iterator % temp_save_every == 0:
                    save_calculation_progress(
                        current_values, best_fit, best_fit_combination, array_to_return, elements, best_additional_parameters, ind=ind)

            return

        for value in arr[depth]:
            current_values.append(float(f'{value:g}'))
            nested_iteration(arr, depth + 1, current_values,
                             additional_parameters, search_value)

            current_values.pop()

    nested_iteration(array_to_return, 0, [],
                     additional_parameters, search_value)

    results = [best_fit, best_fit_coordinates,
               best_fit_combination, mapped_combination, best_additional_parameters]

    if thread_return_value and ind:
        thread_return_value[ind-1] = results

    return results


def save_calculation_progress(current_values, best_fit, best_fit_combination, array_to_return, elements, best_additional_parameters, ind=None):
    global number_of_combinations
    global combintations_calculated
    global additional_parameters

    filename = f'temp/temp_result.json'
    if ind:
        # ind is used for threading so each thread will have separate temp file
        filename = f'temp/temp_result_{ind}.json'

    result = {
        'number_of_combinations': number_of_combinations,
        'combintations_calculated': combintations_calculated,
        'temp_current_values': current_values,
        'temp_best_fit': best_fit,
        'best_fit_combination': best_fit_combination,
        'parameters': dict(zip(elements, [[float(f'{elem:g}') for elem in x] for x in array_to_return])),
        'additional_parameters':  additional_parameters,
        'best_additional_parameters': best_additional_parameters,
    }

    json_object = json.dumps(result, indent=4)
    with open(filename, "w+") as outfile:
        outfile.write(json_object)


def calculate_combination(combination, elements, plot_arrays, additional_parameters, ddH, ind=None):
    mapped_combination = dict(zip(elements, combination))
    # TODO: iterate over zJ
    chi_s = calculate_chi_s(ddH, mapped_combination, ind=ind)
    # coordinates = diagram('red')
    coordinates = []
    try:
        tip = additional_parameters['tip']
        _zJ = additional_parameters['zJ']
        par_p = additional_parameters['par_p']  # % -> part of 1
        par_s = additional_parameters['par_s']
        par_g = additional_parameters['par_g']
        zJ_g = additional_parameters['zJ_g']
        zJ_values = np.arange(float(_zJ['from']), float(
            _zJ['to'])+float(_zJ['step']), float(_zJ['step']))

        for zJ in zJ_values:
            diagram_to_insert = diagram('red')
            chi_t_s = chi_t(temps, chi_s, zJ_g, zJ, par_g, par_s, par_p, tip)
            for w in range(len(temps)):
                diagram_to_insert.append(temps[w], chi_t_s[w])

            temp_additional_parameters = dict(additional_parameters, zJ=zJ)
            coordinates.append([diagram_to_insert, temp_additional_parameters])
    except Exception as e:
        print(str(e))
        showerror(_('Error'), _('Inappropriate parameters values'))
        return -1

    best_correlation_with_additional_parameters = None
    best_coordinates_with_additional_parameters = None

    for coordinate, temp_additional_parameter in coordinates:
        correlation = corr_find(plot_arrays[0], coordinate)
        # if set correlation if not set or current is smaller
        if best_correlation_with_additional_parameters is None or (best_correlation_with_additional_parameters > correlation):
            best_correlation_with_additional_parameters = correlation
            best_coordinates_with_additional_parameters = coordinate
            best_temp_additional_parameter = temp_additional_parameter

    return best_correlation_with_additional_parameters, best_coordinates_with_additional_parameters, best_temp_additional_parameter


def convert_into_2d_array_of_values(param_frames):
    array_to_return = []

    for key in param_frames.keys():
        start = float(param_frames[key]['from'].get())
        to = float(param_frames[key]['to'].get())
        if param_frames[key]['step'].get():
            step = float(param_frames[key]['step'].get())
            decimal_places = len(str(step).split(".")[1])
        np_array = None

        if start == to:
            np_array = np.array([start])
        else:
            np_array = np.around(
                np.arange(start, to + step, step), decimal_places)
        # additionally limit start and stop values
        np_array = np_array.clip(start, to)
        array_to_return.append(np_array)

    return array_to_return


def get_additional_parameters():
    # TODO: zj should be range
    global ex_param_frames
    return {
        'tip': float(ex_param_frames['tip'].get()),
        'zJ': {
            'from': ex_param_frames['zJ']['from'].get(),
            'to': ex_param_frames['zJ']['to'].get(),
            'step': ex_param_frames['zJ']['step'].get()
        },
        'par_p': float(ex_param_frames['par_p'].get())/100.0,  # % -> part of 1
        'par_s': float(ex_param_frames['par_s'].get()),
        'par_g': float(ex_param_frames['par_g'].get()),
        'zJ_g': float(ex_param_frames['zJ_g'].get())
    }


def calculate_chi_s(ddH, combination, ind=None):
    list_param = ''
    list_param_deriv = ''

    for key, value in combination.items():
        try:
            pn = float(value)
        except:
            showerror(_('Error'), _('Empty parameter ')+key)
            return 0
        list_param += key+'='+str(pn)+'\n'
        if key == 'H':
            list_param_deriv += key+'='+str((1+ddH)*pn)+'\n'
        else:
            list_param_deriv += key+'='+str(pn)+'\n'

    list_param += 'Hz=H\nHx=H\nHy=H\n'
    list_param_deriv += 'Hz=H\nHx=H\nHy=H\n'
    # H = float(param_frames['H'].get())
    H = float(combination['H'])
    exec_calc = 1
    if H*ddH < math.pow(10, -8):
        exec_calc = 0
        res = askyesno(_('Attention!'), _('dH is equal to ')+str(H*ddH) +
                       _(', which is too small. This may cause machine mathematics errors. Continue?'))
        if res:
            exec_calc = 1
    if exec_calc == 1:
        eigenvalues = calc(list_param, list_param_deriv, exec_list,
                           exec_file_list, matrix_name_list, parameters)
        chi_s = chi(temps, eigenvalues, H,
                    ddH, list_param, fragments)

        return chi_s


def calculate_derivative(chi_s, zJ_g, zJ, par_g, par_s, par_p, tip):
    # should take list of parameters
    chi_t_s = chi_t(temps, chi_s, zJ_g, zJ, par_g, par_s, par_p, tip)

    coordinates = diagram('red')
    for w in range(len(temps)):
        coordinates.append(temps[w], chi_t_s[w])

    return coordinates


def plot(is_best_fit=True):
    '''Request data calculation for given parameters and draw it. If is_best_fit == False
    only draw yet calculated data'''
    global plot_arrays

    plot_axes()
    if is_best_fit:  # plot best fit
        global additional_parameters
        clean_temp_files()
        additional_parameters = get_additional_parameters()
        best_fit_coordinates = prepare_plot(plot_arrays)
        plot_arrays.append(best_fit_coordinates)
#    plot_arrays = plot_arrays[:1]
    draw_plot(plot_arrays)


def draw_plot(plot_arrays):
    global pic
    pic.delete('diagram')
    basic_line = 1
    for i in plot_arrays:
        j = i.points[0:]  # Local copy to reuse initial array
        el = j.pop(0)
        cur_x = el.x
        cur_y = el.y
        if basic_line == 1:
            word = 'basic_diagram'
            basic_line = 0
        else:
            word = 'diagram'
        while len(j) > 0:
            el = j.pop(0)
            next_x = el.x
            next_y = el.y
            pic.create_line(phys_x(cur_x), phys_y(cur_y), phys_x(
                next_x), phys_y(next_y), fill=i.color, tag=word)
            cur_x = next_x
            cur_y = next_y


def save_fit(coordinates, combination, addional_parameters):
    global saved_fits_number
    fit_data = '# '

    with open('fits/fit'+str(saved_fits_number)+'.csv', 'w+', newline='') as file:
        writer = csv.writer(file)
        for key, value in combination.items():
            try:
                pn = float(value)
            except:
                showerror(_('Error'), _('Empty parameter ')+key)
                return 0
            fit_data += key+'='+str(pn)+', '
        fit_data += '\n# '
        writer.writerow([fit_data])
        temp_line = ''
        for key, value in addional_parameters.items():
            try:
                value = float(value)
            except:
                showerror(_('Error'), _('Empty parameter ')+key)
                return 0
            temp_line += key+'='+str(value)+', '

        fit_data += temp_line
        writer.writerow([temp_line])
        fit_data += l_r['text']+'\n# T\txT\n'
        writer.writerow(['T', 'HiT', 'Hi'])
        for i in coordinates.points:
            writer.writerow([i.x, i.y, math.sqrt(i.y*8)])
            fit_data += str(i.x)+'\t'+str(i.y)+'\t'+str(math.sqrt(i.y*8))+'\n'
    #    print fit_data
        w = open('fits/fit'+str(saved_fits_number)+'.dat', 'w+')
        w.write(fit_data)
        w.close()
        saved_fits_number += 1


def save_ready():
    '''Save final result as fit.dat'''
    s = saved_fits_number-1
    if s < 0:
        pass
        showerror(_('No data!'), _('No data to save'))
    else:
        copy('fits/fit'+str(s)+'.dat', 'fit.dat')
        showinfo(_('Done'), _('Saved as')+' fit.dat')


def clean():
    global plot_arrays, saved_fits_number
    plot_arrays = plot_arrays[:1]
    pic.delete('diagram')
    rmtree('fits')
    os.mkdir('fits')
    saved_fits_number = 0


def rescale():
    global y_coords, x_coords
    try:
        left = float(ey_min.get())
        right = float(ey_max.get())
        if right < 0 or left < 0 or left >= right:
            raise Exception('Left>Right')
        y_coords[0] = left
        y_coords[1] = right
        y_coords[2] = (right-left)/10.0

        leftx = float(ex_min.get())
        rightx = float(ex_max.get())
        if rightx < 0 or leftx < 0 or leftx >= rightx:
            raise Exception('Left>Right')
        x_coords[0] = leftx
        x_coords[1] = rightx
        x_coords[2] = (rightx-leftx)/5.0

        pic.delete('basic_diagram')
        plot(False)
    except:
        showerror(_('Incorrect values!'), _(
            'Inappropriate values of the range limits'))


def corr_find(plot_arrays, testing_plot):
    '''R^2 calculation'''
    first = plot_arrays
    # second = plot_arrays[-1:].pop(0)
    second = testing_plot
    deltas = 0.0
    values = 0.0
    for i in range(len(first.points)):
        th = second.points[i].y
        ex = first.points[i].y
        deltas += (th-ex)*(th-ex)
        values += ex*ex
    return deltas/values


def fill_test_values(array):
    '''
    Fill testing value in UI
    '''

    test_values = {
        'm0_JNiSQ': {'from': 99,  # m0_JNiSQ from -1000 to 1000 (точность 0.1)
                     'to': 100,
                     'step': 0.1},
        'm0_JSQSQ': {'from': 9,  # m0_JSQSQ from -1000 to 1000 (точность 0.1)
                     'to': 10,
                     'step': 0.1},
        'm0_gzNi':  {'from': 2.3,  # m0_gzNi from 2 to 2.5 в 0.01
                     'to': 2.5,
                     'step': 0.01},
        'm0_gSQ':   {'from': 2,  # mogz от 2 до 2.5 (точность в сотую)
                     'to': 2.01,
                     'step': 0.01},
        'H':       {'from': 0.5,
                    'to': 0.5,
                    'step': 0.1},
    }

    test_values = {
        'm0_JCuSQ': {'from': 99,  # m0_JNiSQ from -1000 to 1000 (точность 0.1)
                     'to': 100,
                     'step': 0.1},
        'm0_JSQSQ': {'from': 9,  # m0_JSQSQ from -1000 to 1000 (точность 0.1)
                     'to': 10,
                     'step': 0.1},
        'm0_gzCu':  {'from': 2.3,  # m0_gzNi from 2 to 2.5 в 0.01
                     'to': 2.5,
                     'step': 0.01},
        'm0_gSQ':   {'from': 2,  # mogz от 2 до 2.5 (точность в сотую)
                     'to': 2,
                     'step': 0},
        'H':       {'from': 0.5,
                    'to': 0.5,
                    'step': 0},
    }

    test_values = {

        # best fit value
        # m0_JNiSQ=0.0, m0_JSQ4SQ5=-1.0, m0_JSQSQ=-1.0, m0_gzNi=2.0, m0_gSQ=2.0, H=0.5,
        # ddH=4.0, zJ=-0.3, zJ_g=2.0, par_p=0.0, par_s=0.5, par_g=2.0, tip=0.0002, R2 = 2.3234378888946967


        'm0_JNiSQ': {'from': 0,  # m0_JNiSQ from -1000 to 1000 (точность 0.1)
                     'to': 0.2,
                     'step': 0.1},
        'm0_JSQSQ': {'from': 0,  # m0_JSQSQ from -1000 to 1000 (точность 0.1)
                     'to': 0.2,
                     'step': 0.1},
        'm0_JSQ4SQ5':  {'from': 0,  # m0_gzNi from 2 to 2.5 в 0.01
                        'to': 0.2,
                        'step': 0.1},
        'm0_gzNi':   {'from': 2,  # mogz от 2 до 2.5 (точность в сотую)
                      'to': 2,
                      'step': 0.1},
        'm0_gSQ':   {'from': 2,  # mogz от 2 до 2.5 (точность в сотую)
                     'to': 2,
                     'step': 0},
        'H':       {'from': 0.5,
                    'to': 0.5,
                    'step': 0},
    }

    test_values = {
        'm0_JCuSQ': {'from': 0,  # m0_JNiSQ from -1000 to 1000 (точность 0.1)
                     'to': 300,
                     'step': 0.1},
        'm0_JSQSQ': {'from': -300,  # m0_JSQSQ from -1000 to 1000 (точность 0.1)
                     'to': 0,
                     'step': 0.1},
        'm0_gzCu':  {'from': 1.7,  # m0_gzNi from 2 to 2.5 в 0.01
                     'to': 2.5,
                     'step': 0.01},
        'm0_gSQ':   {'from': 2,  # mogz от 2 до 2.5 (точность в сотую)
                     'to': 2,
                     'step': 0},
        'H':       {'from': 0.5,
                    'to': 0.5,
                    'step': 0},
    }


    for element in test_values:
        array[element]['from'].insert(0, test_values[element]['from'])
        array[element]['to'].insert(0, test_values[element]['to'])
        array[element]['step'].insert(0, test_values[element]['step'])

    return array


def load_fragments():
    global fragments
    f = open("fragments")
    fragments = f.readlines()


def load_source():
    global source
    src = open('source.dat', 'r')
    source = src.readlines()


def load_threads():
    global number_of_threads
    src = open('threads', 'r')
    try:
        number_of_threads = int(src.readlines()[0])
    except Exception as e:
        print('thead file should contains integer number of threads')


def load_symmetry():
    list_of_files = glob.glob('symmetry*')
    for file in list_of_files:
        with open(file) as f:
            symmetry[file.removeprefix('symmetry')] = f.readlines()[0].strip()


def load_param_list():
    for i in fragments:
        j = i.strip()
        if j == '' or j[0:1] == '#':
            pass
        else:
            num = j.split("\t")[0]
            param_file = open('param_list'+num.strip())
            param_list.extend(param_file.readline().split(','))
            param_file.close()

    param_list.append('H')


def load_execution_list():
    global exec_list
    global exec_file_list
    global matrix_name_list
    global fragments
    global parameters

    for i in fragments:
        j = i.strip()
        if j == '' or j[0:1] == "#":
            pass
        else:
            j_arr = j.split("\t")
            num, weight, if_exec = j_arr[0], j_arr[1], j_arr[2]
            if if_exec == "+":
                # sym_num = read_symm(num)
                sym_num = symmetry[num]

                if sym_num == 'isotropic':
                    exec_list.append((float(weight), "z"+num))
                    matrix_name_list.append("mz"+num)
                    exec_file_list.append("mz"+num)
                    exec_file_list.append("mz"+num+'_deriv')
                elif sym_num == 'axial':
                    exec_list.append((float(weight)/3, "z"+num))
                    matrix_name_list.append("mz"+num)
                    exec_file_list.append("mz"+num)
                    exec_file_list.append("mz"+num+'_deriv')

                    exec_list.append((2*float(weight)/3, "x"+num))
                    exec_file_list.append("mx"+num)
                    exec_file_list.append("mx"+num+'_deriv')
                    matrix_name_list.append("mx"+num)
                else:
                    raise Exception("Unknown symmetry: ", sym_num)

    for j in range(len(exec_list)):
        fin = open(exec_list[j][1]+'.inc')
        fcont = fin.read()
        parameters[exec_list[j][1]] = fcont


def continue_execution():
    global plot_arrays
    number_of_threads, parameters, ddH, splits, current_iterations, temp_best_fit = get_temp_data()
    best_fit_coordinates = find_best_fit(
        number_of_threads, parameters, plot_arrays, ddH, splits, temp_best_fit, current_iterations)
    plot_arrays.append(best_fit_coordinates)

    draw_plot(plot_arrays)


def get_temp_files():
    return glob.glob("temp/temp_result_*.json")


def clean_temp_files():
    files = get_temp_files()
    for file in files:
        os.remove(file)


def get_temp_data():
    '''
    read how many temp_result_*.json files in temp folder -> save as number_of_threads
    while reading get parameters.keys() from all :
        1) check if all elements among files are same -> if no raise error: saved values aren't equal
        2) check if elements are same with current model -> if no raise error: save data is not for current model

    '''
    global additional_parameters
    global combintations_calculated
    global number_of_combinations

    combintations_calculated = 0
    splits = []
    parameters = None
    additional_parameters = None
    number_of_combinations = None
    temp_best_fit = None
    number_of_threads = 0
    current_iterations = []

    try:
        ddH = math.pow(10, (-int(ex_param_frames['ddH'].get())))
    except:
        showerror(_('Error'), _('Inappropriate parameter -lg(ddH/H): ') +
                  ex_param_frames['ddH'].get())

    files = get_temp_files()
    for file in files:
        reader = open(file)
        temp_data = json.load(reader)
        number_of_combinations = int(temp_data['number_of_combinations'])

        if additional_parameters is None:
            additional_parameters = temp_data['additional_parameters']
        elif additional_parameters != temp_data['additional_parameters']:
            showerror(_('Error'), ("Additional parameters don't match"))

        if parameters is None:
            parameters = temp_data['parameters'].keys()
        elif parameters != temp_data['parameters'].keys():
            showerror(_('Error'), ("Additional parameters don't match"))

        if temp_best_fit is None or temp_best_fit > temp_data['temp_best_fit']:
            temp_best_fit = temp_data['temp_best_fit']

        combintations_calculated = max(
            temp_data['combintations_calculated'], combintations_calculated)

        splits.append(list(temp_data['parameters'].values()))
        current_iterations.append(temp_data['temp_current_values'])

        number_of_threads += 1

    if number_of_threads != len(files):
        # Number of threads matches with number of files:
        showerror(_('Error'), ("Number of files don't match number of threads"))

    # fix returning parameters
    return number_of_threads, parameters, ddH, splits, current_iterations, temp_best_fit


def load_global_parameters():
    load_fragments()
    load_source()
    load_symmetry()
    load_param_list()
    load_execution_list()
    load_threads()

# ====================================================================
# The main part of the program
# ====================================================================


if __name__ == "__main__":
    try:
        global progress_bar
        global root
        used_font = ('Times', 14, 'normal')  # Widget font
        canv_font = ('Times', 12, 'normal')  # Picture font

        load_global_parameters()

        ld = diagram('green')

        temps = []
        for cnt in source:
            cnts = cnt.strip()
            if cnts[0:1] == '#' or cnts == '':
                pass
            else:
                la = cnts.split('\t')
                ld.append(float(la[0].strip()), float(la[1].strip()))
                temps.append(float(la[0].strip()))
        plot_arrays.append(ld)

        # Preparing directory `fits' to save temporary results
        if os.access('fits', os.F_OK):
            rmtree('fits')
        os.mkdir('fits')

        root = Tk()
        root.title('Stormbreaker: '+_('Parameters fit'))
        pic_n_conf = Frame(root)

        # progress bar added TODO: display actuall percentage
        progress_bar = ttk.Progressbar(
            root, length=1000, mode="determinate", orient="horizontal")
        progress_bar.pack(side=TOP, expand=YES)

        pic_n_conf.pack(side=LEFT, expand=YES)
        l_r = Label(pic_n_conf, text='R2 = ', font=used_font)
        l_r.pack(side=TOP)

        global pic
        pic = Canvas(
            pic_n_conf, width=pic_size[0], height=pic_size[1], bg='white')
        rescaler = Frame(pic_n_conf)
        ey_min = Entry(rescaler, font=used_font)
        l1 = Label(rescaler, text='χT '+_('range from '), font=used_font)
        l2 = Label(rescaler, text=_(' to '), font=used_font)
        ey_max = Entry(rescaler, font=used_font)
        pic.pack(side=TOP)
        rescaler.pack(side=TOP, expand=YES)
        l1.pack(side=LEFT)
        ey_min.pack(side=LEFT)
        l2.pack(side=LEFT)
        ey_max.pack(side=LEFT)

        rescalerx = Frame(pic_n_conf)
        ex_min = Entry(rescalerx, font=used_font)
        l1 = Label(rescalerx, text='T '+_('range from '), font=used_font)
        l2 = Label(rescalerx, text=_(' to '), font=used_font)
        ex_max = Entry(rescalerx, font=used_font)

        b = Button(rescalerx, text=_('Set'), command=rescale, font=used_font)
        pic.pack(side=TOP)
        rescalerx.pack(side=TOP, expand=YES)
        l1.pack(side=LEFT)
        ex_min.pack(side=LEFT)
        l2.pack(side=LEFT)
        ex_max.pack(side=LEFT)
        b.pack(side=LEFT)

        ex_min.insert(0, "0")
        ex_max.insert(0, "300")
        ey_min.insert(0, "0")
        ey_max.insert(0, "30")

        params = LabelFrame(root, text=_('Parameters'), font=used_font)

        plot(False)

        for i in param_list:
            p = magn_param(params, i, is_range=True)
            param_frames[i] = {
                'from': p.from_entry,
                'to': p.to_entry,
                'step': p.step_entry}

        '''
        Fill default parameters for testing purposes
        '''
        param_frames = fill_test_values(param_frames)

        ex_param_frames = {}
        ex_param_frames_l = {}

        ex_params = LabelFrame(root, text=_(
            'Additional parameters'), font=used_font)

        for i in ('ddH', 'zJ', 'zJ_g', 'par_p', 'par_s', 'par_g', 'tip'):
            if i == 'zJ':
                p = magn_param(ex_params, i, is_range=True)
                ex_param_frames[i] = {
                    'from': p.from_entry,
                    'to': p.to_entry,
                    'step': p.step_entry}
            else:
                p = magn_param(ex_params, i)
                ex_param_frames[i] = p.from_entry
                ex_param_frames_l[i] = p.element_label

        ex_param_frames_l['ddH'].config(text='-lg(dH/H)')
        ex_param_frames['ddH'].insert(0, -int(math.log10(diff_const)))

        # ex_param_frames_l['zJ'].config(text='zJ\'')
        ex_param_frames['zJ']['from'].insert(0, -20)
        ex_param_frames['zJ']['to'].insert(0, 0)
        ex_param_frames['zJ']['step'].insert(0, 0.1)

        ex_param_frames_l['par_p'].config(
            text=_('Paramagnetic impurity, % mol.'))
        ex_param_frames['par_p'].insert(0, 0)
        ex_param_frames_l['par_s'].config(text=_('Paramagnetic impurity spin'))
        ex_param_frames['par_s'].insert(0, '0.5')
        ex_param_frames_l['par_g'].config(
            text=_('g of the paramagnetic impurity'))
        ex_param_frames['par_g'].insert(0, '2.0')
        ex_param_frames_l['tip'].config(text=_('TIP'))
        ex_param_frames['tip'].insert(0, 0.0002)
        ex_param_frames_l['zJ_g'].config(text=_('Mediated g for mol. field'))
        ex_param_frames['zJ_g'].insert(0, '2.0')

        params.pack(expand=YES, side=TOP)
        ex_params.pack(side=TOP, expand=YES)

        actions = LabelFrame(root, text=_('Actions'), font=used_font)
        run = Button(actions, font=used_font, text=_('Draw'), command=plot)
        run.pack(side=LEFT, expand=YES)

        butt_clean = Button(actions, font=used_font,
                            text=_('Clean'), command=clean)
        butt_clean.pack(side=LEFT, expand=YES)

        butt_save = Button(actions, font=used_font, text=_(
            'Save the last fit'), command=save_ready)
        butt_save.pack(side=LEFT, expand=YES)

        butt_continue_execution = Button(actions, font=used_font, text=_(
            'Continue execution'), command=continue_execution)
        butt_continue_execution.pack(side=LEFT, expand=YES)
        actions.pack(expand=YES, side=TOP)

        root.mainloop()

    except:
        print(_('Failed on loading: '), sys.exc_info()[1])
