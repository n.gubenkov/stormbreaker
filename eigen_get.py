# This module prepares numeric hamiltonian matrixes on the base of user selected parameters
# and calls `find_eig' external command to calculate eigenvalues

from const import *
from os import linesep

import numpy as np


def matrix_num_print(m, f):
    '''Writes prepared numeric matrix to file to pass it to find_eig command'''
    with open(f+'.num', 'w+') as fp:
        l = str(len(m))+linesep
        fp.write(l)
        for i in m:
            li = '\t'.join(map(lambda x: str(x), i))
            fp.write(li+'\n')


def read_symm(file_prefix):
    a = open("symmetry"+file_prefix.strip())
    return a.readlines()[0].strip()


def calc(e_par, e_par_deriv, exec_list, exec_file_list, matrix_name_list, parameters):
    '''Checks if there are z, x, y matrices and requests calculation of eigenvalues of each 
    (base and derivated) with user-selected parameters'''
    ##########################################

    '''
	read from all files before to access it from different threads without locks

	'''

    eigenvalues = []
    eigenvalues_test = []
    eigenvalues_param = []
    eigenvalues_deriv = []
    # declare parameters from
    exec(e_par)
    for j in range(len(exec_list)):

        fcont = parameters[exec_list[j][1]]

        exec(fcont)  # read file and save it as variable
        array_to_parse = locals()[matrix_name_list[j]]

        eigenvalues_param.append(np.sort(np.linalg.eig(array_to_parse)[0]))

    exec(e_par_deriv)
    for j in range(len(exec_list)):
        fcont = parameters[exec_list[j][1]]

        exec(fcont)  # read file and save it as variable
        derivative_array_to_parse = locals()[matrix_name_list[j]]

        eigenvalues_deriv.append(
            np.sort(np.linalg.eig(derivative_array_to_parse)[0]))

    for ind, item in enumerate(exec_list):
        eigenvalues.append(
            (item[0], eigenvalues_param[ind], eigenvalues_deriv[ind]))

    return eigenvalues
